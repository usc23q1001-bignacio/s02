name = "Bligh"
age = 22
occupation = "College student"
movie = "Knives Out"
rating = 99.99

print(f"I am {name}, and I am {age}, I work as a {occupation}, and my rating for {movie} is {rating}%")

num1 = 2
num2 = 4
num3 = 6

print(num1 * num2)
print(num1 < num3)
num2 += num3
print(num2)